/*
 * MCP2515.c version_00
 *           Pin 4.7 of the MSP430FR5994 is CS pin for SPI
 *  Created on: Dec 26, 2019
 *      Author: SAKBUDAK
 */

#include "mcp2515.h"

//####################################################################################################################################################################################
//                                                                      MCP2515_SPI_init()

void MCP2515_SPI_init(void){
          P5SEL0 = BIT0 + BIT1 + BIT2;                                      // Special module primary module                         // Special function for SCK, MOSI and MISO ...
          P5SEL1 = 0x00;                                                    // ---

          P4DIR |= BIT7;                                                                 // !CS as an output
          P4OUT |= BIT7;                                                                 // ---

          UCB1CTLW0 |= UCSWRST;                                                           // Reset**

          UCB1CTLW0 |= UCCKPL + UCMSB + UCMST + UCMODE_0 + UCSYNC;                        // 3-pin, 8-bit SPI master
          UCB1CTLW0 |= UCSSEL_2;                                                          // SMCLK as 1MHz
          UCB1BRW |= 0x02;                                                                // ...

          UCB1CTLW0 &= ~UCSWRST;                                                          // Initialize USCI state machine **

          __delay_cycles(DELAY_100ms);                                                   // wait 100ms
          while (!(UCB1IFG & UCTXIFG));                                                   // wait for sent
          UCB1TXBUF = 0x00;
}

//####################################################################################################################################################################################
//                                                                     MCP2515_SPI_transmit()
//  SPI-UCB1.
//
// -DATA   : Unsigned 8-bit data record to be sent with UCB1 via SPI
//
// -RETURN : Unsigned 8-bit data record that is received with UCB1 via SPI
//
unsigned char MCP2515_SPI_transmit(unsigned char data)
{
  UCB1TXBUF = data;                                                             // Send data
  while(UCB1STATW & UCBUSY);                                                    // Waiting for busy
  return UCB1RXBUF;                                                             // Return received record
}


//####################################################################################################################################################################################
//                                                                      MCP2515_reset()

//
void MCP2515_reset (void)
{
  MCP2515_CS_LOW;                                               // Start SPI Communication
  MCP2515_SPI_transmit(MCP2515_RESET);
  MCP2515_CS_HIGH;                                             // End SPI Communication

  __delay_cycles(DELAY_100us);
}

//####################################################################################################################################################################################
//                                                                 MCP2515_CanVariable_init()

void MCP2515_CanVariable_init (can_t *can)
{
  char i;
  can->COB_ID ;                     // Determine CAN-ID
  can->status = 0x01;                       //
  can->dlc = CAN_DLC;                       //
  can->rtr = CAN_RTR;                       //
  can->ext = CAN_EXTENDET;                  // CAN type, extended or standard
  for(i = 0; i < CAN_DLC; i++)can->data[i] = 0;
}



//####################################################################################################################################################################################
//                                                                      MCP2515_write()

void MCP2515_write (uint8_t addr, uint8_t data)
{
  MCP2515_CS_LOW;

  MCP2515_SPI_transmit(MCP2515_WRITE);
  MCP2515_SPI_transmit(addr);
  MCP2515_SPI_transmit(data);

  MCP2515_CS_HIGH;

  __delay_cycles(DELAY_1ms);
}

//####################################################################################################################################################################################
//                                                                 MCP2515_write_many_registers()

void MCP2515_write_many_registers(uint8_t addr, uint8_t len, uint8_t *data)
{
  MCP2515_CS_LOW;

  MCP2515_SPI_transmit(MCP2515_WRITE);
  MCP2515_SPI_transmit(addr);

  char i;
  for(i=0; i < len; i++)
  {
    MCP2515_SPI_transmit(*data);
    data++;
  } // for

  MCP2515_CS_HIGH;

  __delay_cycles(DELAY_100us);
}

//####################################################################################################################################################################################
//                                                                     MCP2515_RX_Status()

uint8_t MCP2515_RX_Status(){
    uint8_t data;

    MCP2515_CS_LOW;

    data = MCP2515_SPI_transmit(MCP2515_RX_STATUS);

    MCP2515_CS_HIGH;

    __delay_cycles(DELAY_100us);

    return data;
}
//####################################################################################################################################################################################
//                                                                       MCP2515_read()

uint8_t  MCP2515_read(uint8_t addr)
{
  uint8_t data;

  MCP2515_CS_LOW;

  MCP2515_SPI_transmit(MCP2515_READ);
  MCP2515_SPI_transmit(addr);
  data = MCP2515_SPI_transmit(MCP2515_DUMMY);

  MCP2515_CS_HIGH;

  __delay_cycles(DELAY_100us);

  return data;
}

//####################################################################################################################################################################################
//                                                                MCP2515_read_many_registers()

void MCP2515_read_many_registers(uint8_t addr, uint8_t length, uint8_t *data)
{
  MCP2515_CS_LOW;                                                                

  MCP2515_SPI_transmit(MCP2515_WRITE);             
  MCP2515_SPI_transmit(addr);                     

  char i;
  for(i=1; i < length; i++)                                 
  {
    *data = MCP2515_SPI_transmit(MCP2515_DUMMY);             
    data++;                                                    
  } // for

  MCP2515_CS_HIGH;                                                  

  __delay_cycles(DELAY_100us);                      
}

//####################################################################################################################################################################################
//                                                                     MCP2515_write_id()

void MCP2515_write_id(uint8_t addr, BOOL ext, unsigned long id)
{
  uint16_t canid;
  uint8_t tbufdata[4];

  canid = (unsigned short)(id & 0x0ffff);

  if(ext == TRUE) // if 29-Bit-Identifier (CAN 2.0B)
  {
    tbufdata[MCP2515_EID0] = (uint8_t) (canid & 0xff);
    tbufdata[MCP2515_EID8] = (uint8_t) (canid / 256);
    canid = (uint16_t)(id / 0x10000);
    tbufdata[MCP2515_SIDL] = (uint8_t) (canid & 0x03);
    tbufdata[MCP2515_SIDL] +=  (uint8_t)((canid & 0x1c)*8);
    tbufdata[MCP2515_SIDL] |= MCP2515_TXBnSIDL_EXIDE;
    tbufdata[MCP2515_SIDH] = (uint8_t)(canid / 32);
  }

  else
  {
    tbufdata[MCP2515_SIDH] = (uint8_t)(canid / 8);
    tbufdata[MCP2515_SIDL] = (uint8_t)((canid & 0x07)*32);
    tbufdata[MCP2515_EID0] = 0;
    tbufdata[MCP2515_EID8] = 0;
  } // else

  if(tbufdata[0] == 0xff) return;
  MCP2515_write_many_registers(addr, 4, tbufdata);

  __delay_cycles(DELAY_100us);
}

//####################################################################################################################################################################################
//                                                                     MCP2515_read_id()

void MCP2515_read_id(uint8_t addr, unsigned long* id, char* ext)
{
  uint16_t ID_Low, ID_High;
  if(MCP2515_RXB0SIDL && 0b1000)
      *ext=1;
  else
      *ext=0;

  if(addr == MCP2515_RXB0SIDL)
  {
    ID_Low  = (MCP2515_read(MCP2515_RXB0SIDL) >> 5);
    ID_High = (MCP2515_read(MCP2515_RXB0SIDH) << 3);

    *id = (unsigned long)ID_Low | (unsigned long)ID_High;
  }
  else
  {
    ID_Low  = (MCP2515_read(MCP2515_RXB1SIDL) >> 5);
    ID_High = (MCP2515_read(MCP2515_RXB1SIDH) << 3);

    *id = (unsigned long)ID_Low | (unsigned long)ID_High;
  }
}

//####################################################################################################################################################################################
//                                                                      MCP2515_init()

void MCP2515_init(void)
{
  // ------ 1. Reset den Chip ------------------------------------------------

  MCP2515_reset ();                                                              // After the reset, the chip should start in config mode.

  __delay_cycles(DELAY_10ms);                                                    // give enough time to MCP2515 reset

  // ------ 2. Configure Chip ------------------------------------------

  MCP2515_write(MCP2515_CANCTRL, 0x88);                                          // CAN control register. Go to configuration mode (page 58), but it should actually be there automatically after a restart
  MCP2515_write(MCP2515_CANINTE, 0x03);                                          // Interrupt Enable-Register. Activate for only RX0 and RX1-Interrupts (see page 50)
  MCP2515_write(MCP2515_TXB0CTRL, 0x03);

  // ------ 2a Bit Timing ------------------------------------------------------

  MCP2515_write(MCP2515_CNF1,0x00);                                              // For 8MHz crystal in MCP2515, 500kb, if you want to choose search for http://www.bittiming.can-wiki.info/
  MCP2515_write(MCP2515_CNF2,0x90);                                              //
  MCP2515_write(MCP2515_CNF3,0x02);                                              //

  // ------ 2b Filter usage -----------------------------------------------

  MCP2515_write(MCP2515_RXB0CTRL, 0x64);                                        // turn musk/filters off, receive all messages for RXB0
  MCP2515_write(MCP2515_RXB1CTRL, 0x60);                                        // turn musk/filters off, receive all messages for RXB1
  MCP2515_write(MCP2515_BFPCTRL, 0x00);
  MCP2515_write(MCP2515_TXRTSCTRL , 0x00);

  // ------ 3. Retur to normal mode ---------------------------------

  MCP2515_write(MCP2515_CANCTRL, 0x00);

  __delay_cycles(DELAY_1s);
}

//####################################################################################################################################################################################
//                                                                    MCP2515_bit_modify()

void MCP2515_bit_modify(uint8_t addr, uint8_t mask, uint8_t data)
{
  MCP2515_CS_LOW;

  MCP2515_SPI_transmit(MCP2515_BIT_MODIFY);
  MCP2515_SPI_transmit(addr);
  MCP2515_SPI_transmit(mask);
  MCP2515_SPI_transmit(data);

  MCP2515_CS_HIGH;
  __delay_cycles(DELAY_100us);
}


//####################################################################################################################################################################################
//                                                                     MCP2515_can_tx0()

void MCP2515_can_tx0(can_t *can)
{
  if(can->dlc > 8) can->dlc = 8;                                             // max dlc is 8

  MCP2515_write_id(MCP2515_TXB0SIDH, can->ext, can->COB_ID);

  if (can->rtr == TRUE)                                                    // if there is a remote transmit request, rtr message
  {
      uint8_t command = can->dlc;
      command = command | 0x40;
      if(command == 0x03) return;
      MCP2515_write(MCP2515_TXB0DLC, can->dlc | 0x40);
  } // if (rtr)

  else
  {
    MCP2515_write(MCP2515_TXB0DLC, can->dlc);
    MCP2515_write_many_registers(MCP2515_TXB0D0, can->dlc, can->data);
    MCP2515_write(MCP2515_TXB0CTRL, 0x0B);
  } // else (rtr)
}

//####################################################################################################################################################################################
//                                                                     MCP2515_can_tx1()

void MCP2515_can_tx1(can_t *can)
{
  if(can->dlc > 8) can->dlc = 8;

  MCP2515_write_id(MCP2515_TXB1SIDH, can->ext, can->COB_ID);

  if (can->rtr == TRUE)
  {
      uint8_t befehl = can->dlc;
      befehl = befehl | 0x40;
      if(befehl == 0x03) return;
      MCP2515_write(MCP2515_TXB1DLC, can->dlc | 0x40);
  } // if (rtr)

  else
  {
    MCP2515_write(MCP2515_TXB1DLC, can->dlc);
    MCP2515_write_many_registers(MCP2515_TXB1D0, can->dlc, can->data);
    MCP2515_write(MCP2515_TXB1CTRL, 0x0B);
  } // else (rtr)
}

//####################################################################################################################################################################################
//                                                                     MCP2515_can_tx2()

void MCP2515_can_tx2(can_t *can)
{
  if(can->dlc > 8) can->dlc = 8;

  MCP2515_write_id(MCP2515_TXB2SIDH, can->ext, can->COB_ID);

  if (can->rtr == TRUE)
  {
      uint8_t befehl = can->dlc;
      befehl = befehl | 0x40;
      if(befehl == 0x03) return;
      MCP2515_write(MCP2515_TXB2DLC, can->dlc | 0x40);
  } // if (rtr)

  else
  {
    MCP2515_write(MCP2515_TXB2DLC, can->dlc);
    MCP2515_write_many_registers(MCP2515_TXB2D0, can->dlc, can->data);
    MCP2515_write(MCP2515_TXB2CTRL, 0x0B);
  } // else (rtr)
}

//####################################################################################################################################################################################
//                                                                     MCP2515_can_rx0()

void MCP2515_can_rx0(can_t *can)
{
  MCP2515_read_id(MCP2515_RXB0SIDL, &can->COB_ID, &can->ext);
  can->dlc = MCP2515_read(MCP2515_RXB0DLC);

  char i;
  for(i = 0; i < can->dlc; i++) can->data[i] = MCP2515_read(MCP2515_RXB0D0+i);
  can->status = can->data[0];

  MCP2515_clear_rx0();
  MCP2515_int_clear();

  __delay_cycles(DELAY_1ms);
}

//####################################################################################################################################################################################
//                                                                     MCP2515_can_rx1()

void MCP2515_can_rx1(can_t *can)
{
  MCP2515_read_id(MCP2515_RXB1SIDL, &can->COB_ID, &can->ext);
  can->dlc = MCP2515_read(MCP2515_RXB1DLC);

  char i;
  for(i = 0; i < can->dlc; i++) can->data[i] = MCP2515_read(MCP2515_RXB1D0+i);
  can->status = can->data[0];

  MCP2515_clear_rx1();
  MCP2515_int_clear();

  __delay_cycles(DELAY_1ms);
}

//####################################################################################################################################################################################
//                                                                     MCP2515_clear_rx0()

void MCP2515_clear_rx0(void)                                    // used to clear Interrupt flag for 12.pin of MCP2515
{
  MCP2515_bit_modify(MCP2515_CANINTF, MCP2515_RX0IF, 0x00);
}

//####################################################################################################################################################################################
//                                                                     MCP2515_clear_rx0()

void MCP2515_clear_rx1(void)
{
  MCP2515_bit_modify(MCP2515_CANINTF, MCP2515_RX1IF, 0x00);
}

//####################################################################################################################################################################################
//                                                                     MCP2515_int_clear()

void MCP2515_int_clear(void)
{
  MCP2515_write(MCP2515_CANINTF, MCP2515_CANINTF_ALL_DISABLE);
}






