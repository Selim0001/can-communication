# CAN Communication

CAN communication with MSP430FR5994 by using MCP2515.

-This is the first version of my code there may be some deficiency according to your deployed. I will update after developing each.

-This codes works with only MSP430FR58xx, MSP430FR59xx, and MSP430FR6xx Family. If you want to use it with another MSP family you need to configure some parameters such as, Pins, CLK etc. which are related to SPI

-Code adjusted for MCP2515 to receive all CAN messages whether it is related to you or not.

-Don't forget termination resistance if you use 1,5m or more bus length.