// MSP430FR5994 - CAN Communication - MCP2515
// ---------------------------------
//    Pin Configuration for MCP2515
//    P5.3     -   !INT   -   used for rx interruption
//    P5.2     -   SCK    -   clock
//    P5.0     -   MOSI   -   master out
//    P5.1     -   MISO   -   master in
//    P4.7     -   !CS    -   chip select for SPI
//
//   written by Selim AKBUDAK
// *****************************************************************************

#include <msp430.h>
#include <stdint.h>  // for uint data types
#include <MCP2515.h>


int a=2;
int b=0;
int c=0;
//****************************************************************************************
uint8_t address = MCP2515_CNF2;
uint8_t data_write = 0x90;
uint8_t data_read = 0x00;
uint8_t data[8][16] = {{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // All MCP2515 Registers
                       {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // to see what happens
                       {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // inside of MCP2515
                       {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // see page 63
                       {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},
                       {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},
                       {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},
                       {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}};
uint8_t AD[8] = {0x00,0x20,0x30,0x40,0x50,0x60,0x70};
uint8_t status = 0x0;
//***************************************************************************************************

can_t can_tx;                                                                    // CAN-tx variable from struct
can_t can_rx;                                                                    // CAN-rx variable from sturct
/**
 * main.c
 */
int main(void)
{

	WDTCTL = WDTPW | WDTHOLD;	// stop watchdog timer


	PM5CTL0 &= ~LOCKLPM5;   //
	P1DIR |= BIT0 + BIT1;   // For indication LEDs green and red
	P1OUT &= 0x00;          //

	// --- MSP430-Interrupt Pin -----------------------------------
	P5OUT |=  BIT3;                                                                // P5.3 set
	P5REN |= BIT3;                                                                 // P5.3 Pullup
	P5IE |= BIT3;                                                                  // P5.3 activate interrupt for only this pin
	P5IES |= BIT3;                                                                 // P5.3 Hi/lo Edge
	P5IFG &= ~BIT3;                                                                // Interrupt flag is initially cleared


	MCP2515_SPI_init();
	MCP2515_init();
	SPI_TEST();
	MCP2515_CanVariable_init (&can_tx);                                            // initialization of tx variable
	MCP2515_CanVariable_init (&can_rx);                                            // initialization of rx variable
	can_tx.COB_ID = 0x5dc;
	can_tx.data[1] = 0x12;

	data_read = MCP2515_read(0x2B);                                                // get data from 2B reg to compare written data before

	_EINT();                                                                       // Activate Interrupts
	 int d=0x00;
	  while(1)  // -------------------------// Endless loop
	  {

	    P1OUT ^= BIT0;  // P1.0 Togglen
	    __delay_cycles(DELAY_100ms);
	    can_tx.data[1] = d;                           // to send permanently increasing data in 2nd byte
	    d++;
	    MCP2515_can_rx0(&can_rx);
	    char i;
	    char j;
	    for(i=0;i<8;i++)                                            // get the registers of MCP2515
	        for(j=0;j<16;j++)
	            data[i][j] = MCP2515_read(AD[i]+j);

	  status =  MCP2515_RX_Status();


	  } // while
	return 0;
}
// Port 5: Interrupt-Service-Routine
#pragma vector=PORT5_VECTOR
__interrupt void Port_5(void)
 {
    P1OUT ^= BIT1;                                                             // when massage is received, change the led pos.
  MCP2515_can_rx0(&can_rx);                                                    // read data from RX0 channel
  __delay_cycles(DELAY_10ms);                                                  // wait 10ms
  MCP2515_can_tx0(&can_tx);                                                    // Sent the received data back (Echo)
  P5IFG &= ~BIT3;                                                              // P1.4 IFG clear again
}



// FIRST TEST, IF IT  WORKS WITHOUT ANY PROBLEM THEN GREEN LED LIGHTS
int SPI_TEST(void){
    data_read = MCP2515_read(address);
    if(a == 1 || data_read == data_write){
            P1OUT |= BIT1;
            P1OUT &= ~BIT0;}
        else {
            P1OUT |= BIT0;
            P1OUT &= ~BIT1;}
        __delay_cycles(DELAY_10ms);
}

